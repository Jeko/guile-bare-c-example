/*
  https://www.gnu.org/software/guile/manual/html_node/A-Sample-Guile-Main-Program.html#Building-the-Example-with-Autoconf
  https://gist.github.com/ncweinhold/9724254
 */
#include <libguile.h>

 int
 main (int argc, char **argv)
 {
   scm_init_guile();
   scm_c_primitive_load("script.scm");
   return 0;
 }
